from django.contrib import admin

# for feature 11, added the line below
from tasks.models import Task

# Register your models here.

# for Feature 11, added the lines below:


@admin.register(Task)
class TaskAdmin(admin.ModelAdmin):
    list_display = [
        "name",
        "start_date",
        "due_date",
        "is_completed",
        "project",
        "assignee",
        "id",
    ]
