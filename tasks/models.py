from django.db import models

# feature 11 had to add line below to import User (as property of
# of task class below)
from django.contrib.auth.models import User

# feature 11 had to add line below to import Project
from projects.models import Project

# Create your models here.

# feature 11. create task model in the tasks Django app.
# the task model should have the following attributes:
# name, start_date, due_date, is_completed, project, assignee


class Task(models.Model):
    name = models.CharField(max_length=200)
    start_date = models.DateTimeField()
    due_date = models.DateTimeField()
    is_completed = models.BooleanField(default=False)
    notes = models.TextField()
    project = models.ForeignKey(
        Project,
        related_name="tasks",
        on_delete=models.CASCADE,
    )
    assignee = models.ForeignKey(
        User,
        null=True,
        related_name="tasks",
        on_delete=models.CASCADE,
    )

    # feature 11 didn't require this, but I added it:
    def __str__(self) -> str:
        return self.name


# make migrations and migrate your database before you run tests!
