from django.shortcuts import render, redirect

# For Feature 15, added import Task below:
from tasks.models import Task

# For Feature 15, added login_required line below:
from django.contrib.auth.decorators import login_required

# For Feature 15, added import TaskForm line below:
from tasks.forms import TaskForm

# Create your views here.

# For Feature 15, create view for the Task model
# the view must only be accessible by people who are logged in


@login_required
def create_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            task = form.save()
            task.save()
            return redirect("list_projects")
    else:
        form = TaskForm()
    context = {
        "form": form,
    }
    return render(request, "tasks/create.html", context)


# For Feature 16, create view for the Task model
# view must only be accessible by people who are logged in

@login_required
def show_my_tasks(request):
    tasks = Task.objects.filter(assignee=request.user)
    context = {
        "show_my_tasks": tasks,
    }
    return render(request, "tasks/list.html", context)
