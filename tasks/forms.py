# Created forms.py for Feature 15
# For Feature 15, added import ModelForm line below
# For Feature 15, added import Task line below

from django.forms import ModelForm
from tasks.models import Task

# For Feature 15, added class TaskForm below:


class TaskForm(ModelForm):
    class Meta:
        model = Task
        fields = [
            "name",
            "start_date",
            "due_date",
            "project",
            "assignee",
            "notes",
        ]
