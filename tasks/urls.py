# for all tasks app features, added import path below
# for feature 15, added import create_task below
# for feature 16, added import show_my_tasks below
from django.urls import path
from tasks.views import (
    create_task,
    show_my_tasks,
)

# for feature 15, added create_task below
# for feature 16, added show_my_tasks below

urlpatterns = [
    path("create/", create_task, name="create_task"),
    path("mine/", show_my_tasks, name="show_my_tasks"),
]
