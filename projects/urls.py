# for all project app features, added import path below
# for feature 5, added import list_projects below
# for feature 13, added show_project below
# for feature 14, added create_project below
# for feature 15, DO I NEED to import from tasks.views import create_task (below)?
from django.urls import path
from projects.views import (
    list_projects,
    show_project,
    create_project,
)

# for feature 5, added list_projects below
# for feature 13, added show_project below
# for feature 14, added create_project below

urlpatterns = [
    path("", list_projects, name="list_projects"),
    path("<int:id>/", show_project, name="show_project"),
    path("create/", create_project, name="create_project"),
]
