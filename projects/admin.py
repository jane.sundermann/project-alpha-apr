from django.contrib import admin

# For Feature 4, added the line below
from projects.models import Project

# Register your models here.

# For Feature 4, added the lines below:


@admin.register(Project)
class ProjectAdmin(admin.ModelAdmin):
    list_display = [
        "name",
        "description",
        "owner",
        "id",
    ]
