# Created forms.py for Feature 14
# For Feature 14, added import ModelForm line below
# For Feature 14, added import Project line below

from django.forms import ModelForm
from projects.models import Project

# For Feature 14, added class ProjectForm below:


class ProjectForm(ModelForm):
    class Meta:
        model = Project
        fields = [
            "name",
            "description",
            "owner",
        ]
