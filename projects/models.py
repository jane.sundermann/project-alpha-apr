from django.db import models

# feature 3 had to addlinebelowtoimportUser(touseUserinownerpropertyofProjclassbelow)
from django.contrib.auth.models import User

# Create your models here.

# For Feature 3: create a Project model in the project Django
# app. The Project model should have the following attributes:
# name (String, max length 200 char)
# description (String, no max length)
# owner (foreign key to User, refers to auth.Usermodel, related
# name= "projects", on delete cascade, null = True)


class Project(models.Model):
    name = models.CharField(max_length=200)
    description = models.TextField(blank=False)
    owner = models.ForeignKey(
        User,
        related_name="projects",
        on_delete=models.CASCADE,
        null=True,
    )

    # write a def __str__(self) method that returns the name of the
    # project (below)

    def __str__(self) -> str:
        return self.name


# make migrations and migrate your database
