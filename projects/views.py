from django.shortcuts import render, redirect, get_object_or_404

# for Feature 5, added import Project below:
from projects.models import Project

# for Feature 8 and 13, added login_required line below:
from django.contrib.auth.decorators import login_required

# for Feature 14, added import ProjectForm line below:
from projects.forms import ProjectForm

# Create your views here.

# For Feature 5, created a list view for the Project model.
# Create view that will get all of the instances of the
# Project model and put them in the context for the template.

# Feature 8
# added @login_required to protect list view for the Project
# model so that only a person who has logged in can access it
# changed queryset of view to filter the Project objects where
# members equals the logged in users. So instead of .all(),
# used .filter(owner=request.user).


@login_required
def list_projects(request):
    projects = Project.objects.filter(owner=request.user)
    context = {
        "list_projects": projects,
    }
    return render(request, "projects/list.html", context)


# Feature 13
# Create a view that shows the details of a particular project
# A user must be logged in to see the view


@login_required
def show_project(request, id):
    tasks = get_object_or_404(Project, id=id)
    context = {
        "show_projects": tasks,
    }
    return render(request, "projects/detail.html", context)


# Feature 14 - Create view that allows someone to go from
# the project list page to a page that allows them to create
# a new project.
# A person must be logged in to see the view


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            project = form.save()
            project.save()
            return redirect("list_projects")
    else:
        form = ProjectForm()
    context = {
        "form": form,
    }
    return render(request, "projects/create.html", context)
