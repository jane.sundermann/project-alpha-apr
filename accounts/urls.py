# for feature 7 added first line of code and user_login(below):
# for feature 9 added user_logout line of code below
# for feature 10, added signup line of code below
from django.urls import path
from accounts.views import signup, user_login, user_logout

# for feature 7, added line of code (below):
# for feature 9 added user_logout line of code (below):
# for feature 10, added signup line of code cl

urlpatterns = [
    path("login/", user_login, name="login"),
    path("logout/", user_logout, name="logout"),
    path("signup/", signup, name="signup"),
]
