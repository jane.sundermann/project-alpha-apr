# for feature 7, created this file and added following lines of code
from django import forms

# for feature 7, create form that has 2 fields in the accounts Django app with:
# a username field (charfield max length 150 char)
# pw field (charfield max length 150 char, PasswordInput as its widget


class LoginForm(forms.Form):
    username = forms.CharField(max_length=150)
    password = forms.CharField(
        max_length=150,
        widget=forms.PasswordInput,
    )


# feature 10 signup form
# by default, CharField shows as a text input;
# we want to override that for password
# we do that with the widget argument


class SignUpForm(forms.Form):
    username = forms.CharField(max_length=150)

    password = forms.CharField(
        max_length=150,
        widget=forms.PasswordInput,
    )
    password_confirmation = forms.CharField(
        max_length=150,
        widget=forms.PasswordInput,
    )
