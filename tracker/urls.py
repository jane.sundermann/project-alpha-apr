"""
URL configuration for tracker project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin

# for feature 5, added "include" in line below
from django.urls import path, include

# for feature 6, added render redirect line below:
from django.shortcuts import redirect


# for feature 6, added request below:
def redirect_to_home(request):
    return redirect("list_projects")


# for feature 5, added "projects/" line below
# for feature 6, added "" line below
# for feature 7, added "accounts/" line below
# for feature 15, added "tasks/" line below
urlpatterns = [
    path("projects/", include("projects.urls")),
    path("", redirect_to_home, name="home"),
    path("admin/", admin.site.urls),
    path("accounts/", include("accounts.urls")),
    path("tasks/", include("tasks.urls")),
]
